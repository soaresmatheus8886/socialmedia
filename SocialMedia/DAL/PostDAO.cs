﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.DAL
{
    public class PostDAO
    {
        private readonly Context _context;

        public PostDAO(Context context) => _context = context;

        public List<Post> ListPosts() => _context.Posts.Include(post => post.user).OrderBy(x => x.Id).ThenByDescending(x => x.Id).ToList();


        public bool CreatePost(Post post)
        {
            try
            {
               
                _context.Add(post);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
           
        }
    }
}
