﻿using Microsoft.AspNetCore.Mvc;
using SocialMedia.DAL;
using SocialMedia.Models;
using System.Linq;

namespace SocialMedia.Controllers
{
    public class UserController : Controller
    {

        private readonly UserDAO _userDAO;

        public UserController(UserDAO userDAO)
        {
          
            _userDAO = userDAO;
           
        }
        public IActionResult HomePage(string email,string senha)
        {
         
     
            return View();
        } 
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult UserRegister() => View();

         

        [HttpPost]
        public IActionResult UserRegister(string
            txtName, string txtEmail,
            string txtBornDate, string txtCpf, string txtPhone,
            string txtGenre, string txtState, string txtCity)
        {
            User user = new User
            {
                name = txtName,
                email = txtEmail,
                bornDate = txtBornDate,
                cpf = txtCpf,
                phone = txtPhone,
                Genre = txtGenre,
                state = txtState,
                city = txtCity
            };
            _userDAO.UserRegister(user);


            return RedirectToAction("Index", "User");
        }
    }
}
